package com.development.dianrie.practica4_pizzeria

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast

import kotlinx.android.synthetic.main.activity_pedido.*
import kotlinx.android.synthetic.main.content_pedido.*


class PedidoActivity : AppCompatActivity() {
    var layoutManager: RecyclerView.LayoutManager? = null
    var adapter: RecyclerView.Adapter<RecyclerAdapterPedido.ViewHolder>? = null
    override fun onResume() {
        super.onResume()
        //Si no hay nada en pedido se redirige automaticamenta al Main Activity
        if (RecyclerAdapterPedido.itemsPedido.isEmpty()) {
            Toast.makeText(this,"El carrito está vacio",Toast.LENGTH_LONG).show()
            var intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
        // si le das a atras desde otro activiti se recarga la lista del pedido por si se han añadido nuevos item
        adapter?.notifyDataSetChanged()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pedido)
        setSupportActionBar(toolbarPedido)

       // -------------------------------------------------------------------
            // Pongo en el toolbar el precio final
            // calculo el precio final
        var precioFinal: Double = 0.0
            for (i in RecyclerAdapterPedido.itemsPedido){
                precioFinal += i.precioFinal
        }
           toolbarPedido.title = "Total: $precioFinal€"
            //RecyclerView -----------------------------------------
            //Inicializamos

            layoutManager = LinearLayoutManager(this)
            adapter = RecyclerAdapterPedido()

            //Asignamos al RecyclerView
            recyclerViewPedido.layoutManager =  layoutManager
            recyclerViewPedido.adapter = adapter

            //------------------------------------------------------
        //Boton confirmar pedido termina el pedido

        fab.setOnClickListener { view ->
            fab.visibility = View.INVISIBLE

            var snack = Snackbar.make(coordinatorLayout,"¿QUIERES CONFIRMAR TU PEDIDO?",Snackbar.LENGTH_LONG)
            snack.setAction("OK"){ Toast.makeText(this," El pedido a sido realizado con exito \n Puede realizar otro pedido", Toast.LENGTH_LONG).show()
                RecyclerAdapterPedido.itemsPedido.removeAll { true }
                var intent = Intent(this,MainActivity::class.java)
                startActivity(intent)
            }
            snack.setActionTextColor(Color.GREEN)

                .setCallback(object : Snackbar.Callback() {
                    override fun onDismissed(snackbar: Snackbar?, event: Int) {
                        super.onDismissed(snackbar, event)
                        if (event != Snackbar.Callback.DISMISS_EVENT_ACTION) {
                            // Close activity, only if snackbar was dismissed not pressing action button
                            fab.visibility = View.VISIBLE
                            //finish()
                        }
                    }
                })
                .show()
            snack.show()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_pedido, menu)
        return true
    }
    //Se utiliza el menu superior con un icono para borrar el pedido completo
    //se muestra un snackbar para verificar si queremos borrar el pedido
    //se oculta y se muestra el boton de fuinalizar pedido
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings ->  {
                fab.visibility = View.INVISIBLE
                Snackbar.make(coordinatorLayout,"¿QUIERE CANCELAR TU PEDIDO?", Snackbar.LENGTH_LONG)
                    .setAction("BORRAR"){
                        Toast.makeText(this,"Su pedido ha sido cancelado", Toast.LENGTH_LONG).show()
                        RecyclerAdapterPedido.itemsPedido.removeAll { true }
                        var intent = Intent(this,MainActivity::class.java)
                        startActivity(intent)
                    }
                    .setCallback(object : Snackbar.Callback() {
                        override fun onDismissed(snackbar: Snackbar?, event: Int) {
                            super.onDismissed(snackbar, event)
                            if (event != Snackbar.Callback.DISMISS_EVENT_ACTION) {
                                // Close activity, only if snackbar was dismissed not pressing action button
                                fab.visibility = View.VISIBLE
                                //finish()
                            }
                        }
                    })
                    .show()
                true
            }
            R.id.add ->  {
                var intent = Intent(this,MainActivity::class.java)
                startActivity(intent)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

}
