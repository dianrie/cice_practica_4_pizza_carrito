package com.development.dianrie.practica4_pizzeria

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog
import com.github.javiersantos.materialstyleddialogs.enums.Style


class RecyclerAdapterPedido : RecyclerView.Adapter<RecyclerAdapterPedido.ViewHolder>() {
    //Data Source
    //Creo un array de objetos ItemPedido accesible desde todos los Activity
    companion object {
        //RecyclerAdapterPedido.itemsPedido.add(ItemPedido("carne","Grande","",10.0))
        var itemsPedido = mutableListOf<ItemPedido>()
    }


    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): RecyclerAdapterPedido.ViewHolder {
        val vistaCelda = LayoutInflater.from(p0.context).inflate(R.layout.card_layout_pedido,p0,false)
        return ViewHolder(vistaCelda)
    }

    override fun getItemCount(): Int {
        return itemsPedido.size
    }

    override fun onBindViewHolder(p0: RecyclerAdapterPedido.ViewHolder, p1: Int) {
        var datos = RecyclerAdapter()

        p0.nombrePizza.text = datos.nombresPizza[itemsPedido[p1].posicion]
        p0.precioPizza.text = itemsPedido[p1].precioFinal.toString()
        p0.imagenPizza.setImageResource(datos.nombresImagenes[itemsPedido[p1].posicion])
        p0.textoDescripcion = datos.descripcionesPizza[itemsPedido[p1].posicion]
        p0.tamañoPizza.text = itemsPedido[p1].tamaño
        p0.extraPizza.text = itemsPedido[p1].extras
        p0.imagenFondo = datos.nombresImagenes[itemsPedido[p1].posicion]

    }

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        var nombrePizza: TextView
        var precioPizza: TextView
        var imagenPizza: ImageView
        var textoDescripcion: String
        var tamañoPizza: TextView
        var extraPizza: TextView


        var imagenFondo: Int
        var intent = Intent(itemView.context, FormularioActivity::class.java)
        init {
            nombrePizza = itemView.findViewById(R.id.textView)
            precioPizza = itemView.findViewById(R.id.textView3)
            imagenPizza = itemView.findViewById(R.id.imageView)
            tamañoPizza = itemView.findViewById(R.id.textView8)
            extraPizza = itemView.findViewById(R.id.textView12)
            textoDescripcion = ""

            imagenFondo = R.drawable.carne

            //Listener para la imagen que muestre la descripcion de la pizza
            imagenPizza.setOnClickListener {

                val alerta = MaterialStyledDialog.Builder(itemView.context)
                alerta.setTitle("Ingredientes:")
                alerta.setDescription(textoDescripcion)

                alerta.setHeaderDrawable(imagenFondo)
                alerta.setHeaderColor(R.color.mtrl_btn_bg_color_disabled)

                alerta.setStyle(Style.HEADER_WITH_ICON)

                //alerta.setNeutralText("Cancelar")
                //alerta.onNeutral { _, _ ->  }

                alerta.setPositiveText("OK")
                alerta.onPositive { dialog, which ->
                }
                alerta.show()
            }
            //Celda Listener para si se seleciona la celda
            itemView.setOnClickListener {
                //Extras
                //Toast.makeText(itemView.context,"Elige un tamaño y los extras que quieras",Toast.LENGTH_LONG).show()

                }

        }
    }
}