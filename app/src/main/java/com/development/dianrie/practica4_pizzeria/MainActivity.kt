package com.development.dianrie.practica4_pizzeria

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast

import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*

class MainActivity : AppCompatActivity() {

    var layoutManager: RecyclerView.LayoutManager? = null
    var adapter: RecyclerView.Adapter<RecyclerAdapter.ViewHolder>? = null
    var itemMenu: Menu? = null
    override fun onResume() {
        super.onResume()
        if (itemMenu != null){
            if (RecyclerAdapterPedido.itemsPedido.isEmpty()) {
                itemMenu!!.getItem(0).setIcon(R.drawable.carrito)

            }else{
                itemMenu!!.getItem(0).setIcon(R.drawable.carritolleno2)
            }
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbarPedido)

        //Pongo selecciona una pizza en el titulo
        toolbarPedido.title = "Seleccione su Pizza"

        //RecyclerView -----------------------------------------
        //Inicializamos
        layoutManager = LinearLayoutManager(this)
        adapter = RecyclerAdapter()

        //Asignamos al RecyclerView
        miRecyclerView.layoutManager =  layoutManager
        miRecyclerView.adapter = adapter
        //------------------------------------------------------

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        //TODO Necesito acceder a la propiedad icon del menu_main para poder cambiarla
        itemMenu=menu
        if (RecyclerAdapterPedido.itemsPedido.isEmpty()) {
            menu.getItem(0).setIcon(R.drawable.carrito)

        }else{
            menu.getItem(0).setIcon(R.drawable.carritolleno2)
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        //Compruebo si el carrito esta lleno y pongo la foto correspondiente
        return when (item.itemId) {
            R.id.action_settings ->  {
                if (RecyclerAdapterPedido.itemsPedido.isEmpty()) {
                    Toast.makeText(this,"El carrito está vacio",Toast.LENGTH_LONG).show()
                }else{
                    var intent = Intent(this, PedidoActivity::class.java)
                    startActivity(intent)
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
