package com.development.dianrie.practica4_pizzeria

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity;
import android.transition.AutoTransition
import android.transition.ChangeBounds
import android.transition.Scene
import android.transition.TransitionManager
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog
import com.github.javiersantos.materialstyleddialogs.enums.Style
import kotlinx.android.synthetic.main.activity_formulario.*
import kotlinx.android.synthetic.main.activity_main.*

import kotlinx.android.synthetic.main.content_formulario.*

class FormularioActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_formulario)
        setSupportActionBar(toolbarFormulario)

        //Instancio la clase Recicler para tener acceso al los datos de las pizzas
        var datos = RecyclerAdapter()

        //Creo una variable llamada posicion para guardar la posicion de la pizza seleccionada en el Main Activity
        var posicion = intent.getStringExtra("Posicion").toInt()

        //Pongo el nombre de la pizza seleccionada en el titulo
        toolbarFormulario.title = datos.nombresPizza[posicion]
        //Muestro la imagen de la pizza seleccionada
        imageView2.setImageResource(datos.nombresImagenes[posicion])


        //si haces click en la imagen se muestran los ingredientes base de la pizza
        imageView2.setOnClickListener {
            //Toast.makeText(this, datos.descripcionesPizza[posicion], Toast.LENGTH_LONG).show()
            val alerta = MaterialStyledDialog.Builder(this)
            alerta.setDescription(datos.descripcionesPizza[posicion] )
            alerta.setHeaderDrawable(datos.nombresImagenes[posicion])
            alerta.setHeaderColor(R.color.mtrl_btn_bg_color_disabled)

            alerta.setPositiveText("Ok")
            alerta.onPositive { dialog, which ->
            }
            alerta.show()
        }
        //Se muestra el precio base
        var precioBase: Double = datos.preciosPizza[posicion]
        textView6.text = precioBase.toString()

        //Seleccion de Tamaño calcular precio

        var precioTamaño: Double = 0.0
        var precioExtras: Double = 0.0
        var tamaño = "Pequeña"
        var precioFinal: Double = 0.0
        var ingredientes: String
        radioGroup.setOnCheckedChangeListener { group, checkedId ->

             when (checkedId) {
                R.id.pequeña -> {
                    precioTamaño = 0.0
                    textView6.text = (precioExtras + precioBase + precioTamaño).toString()
                    tamaño = "Pequeña"
                }
                R.id.mediana -> {
                     precioTamaño = 2.0
                     textView6.text = (precioExtras + precioBase + precioTamaño).toString()
                    tamaño = "Mediana"
                 }
                R.id.grande -> {
                    precioTamaño = 3.0
                    textView6.text = (precioExtras + precioBase + precioTamaño).toString()
                    tamaño = "Grande"
                }
                else-> precioTamaño = 0.0

            }
        }

        // Seleccion de ingredientes extra calcular precio
        //Queso
        checkBox.setOnCheckedChangeListener { buttonView, isChecked ->
            precioExtras = validarIngredienteExtra(precioExtras,precioBase,precioTamaño,0.30,isChecked)
        }
        //Atún
        checkBox2.setOnCheckedChangeListener { buttonView, isChecked ->
            precioExtras = validarIngredienteExtra(precioExtras,precioBase,precioTamaño,0.20,isChecked)
        }
        //Jamón
        checkBox3.setOnCheckedChangeListener { buttonView, isChecked ->
            precioExtras = validarIngredienteExtra(precioExtras,precioBase,precioTamaño,0.40,isChecked)
        }
        //Pollo
        checkBox4.setOnCheckedChangeListener { buttonView, isChecked ->
            precioExtras = validarIngredienteExtra(precioExtras,precioBase,precioTamaño,0.60,isChecked)
        }
       //Aceitunas
        checkBox5.setOnCheckedChangeListener { buttonView, isChecked ->
            precioExtras = validarIngredienteExtra(precioExtras,precioBase,precioTamaño,0.50,isChecked)
        }
        //Peperoni
        checkBox6.setOnCheckedChangeListener { buttonView, isChecked ->
            precioExtras = validarIngredienteExtra(precioExtras,precioBase,precioTamaño,0.70,isChecked)
        }

        //Boton añadir al carrito muestra un Alerta con el resumen del pedido
        action_button.setOnClickListener {
            precioFinal = precioBase + precioExtras + precioTamaño

            val alerta = MaterialStyledDialog.Builder(this)
            alerta.setTitle("Pizza de: ${datos.nombresPizza[posicion]}")
            alerta.setDescription("Tamaño: $tamaño \n Ingredientes Extra: ${ingredientesSeleccionados()} \n\n Precio: ${precioFinal}€" )
            alerta.setIcon(R.drawable.trozo)
            alerta.setHeaderDrawable(R.drawable.mesa)
            alerta.setStyle(Style.HEADER_WITH_ICON)

            alerta.setNegativeText("Cancelar")
            alerta.onNegative { dialog, which ->
                Toast.makeText(this,"No se añadio al carrito",Toast.LENGTH_LONG).show()
            }
            alerta.setPositiveText("Añadir")
            alerta.onPositive { dialog, which ->
                //Cambio el icono del carrito a lleno y lo animo cambiando entre dos item menu
              // TransitionManager.beginDelayedTransition(toolbarFormulario)
                itemMenu!!.getItem(1).setVisible(true)
                itemMenu!!.getItem(0).setVisible(false)
                if (RecyclerAdapterPedido.itemsPedido.isEmpty()) {
                    itemMenu!!.getItem(1).setIcon(R.drawable.carrito)
                }else{
                    itemMenu!!.getItem(1).setIcon(R.drawable.carritolleno2)
                }

                val autoTransition = AutoTransition()
                autoTransition.setDuration(200)
                TransitionManager.beginDelayedTransition(toolbarFormulario, autoTransition)

                itemMenu!!.getItem(0).setIcon(R.drawable.carritolleno2)
                itemMenu!!.getItem(1).setVisible(false)
                itemMenu!!.getItem(0).setVisible(true)

                //se crea un objeto ItenPedido con los datos de la piza seleccionada y se añade al array del carrito
                RecyclerAdapterPedido.itemsPedido.add(ItemPedido(posicion,tamaño,ingredientesSeleccionados(),precioFinal))
                //Toast.makeText(this,"Se añadio al carrito",Toast.LENGTH_LONG).show()
            }
            alerta.show()

        }

    }
    //Se Crea una funcion para validar los ingredientes extra que se seleccionan y mostar el precio final
    fun validarIngredienteExtra (precioExtras:Double, precioBase:Double, precioTamaño:Double, extra:Double, isChecked:Boolean):Double{
        var precioExtra = precioExtras
        if(isChecked){
            precioExtra += extra
            textView6.text = (precioExtra + precioBase + precioTamaño).toString()
        }else{
            precioExtra -=  extra
            textView6.text = (precioExtra + precioBase + precioTamaño).toString()
        }
        return precioExtra
    }
    //funcion que comprueba que ingredientes extra se han seleccionado para mostrarlos en el resumen
    fun ingredientesSeleccionados ():String{
        var ingredientes: String = ""
        if (checkBox.isChecked){
            ingredientes += "Queso, "
        }
        if (checkBox2.isChecked){
            ingredientes += "Atún, "
        }
        if (checkBox3.isChecked){
            ingredientes += "Jamón, "
        }
        if (checkBox4.isChecked){
            ingredientes += "Pollo, "
        }
        if (checkBox5.isChecked){
            ingredientes += "Aceitunas, "
        }
        if (checkBox6.isChecked){
            ingredientes += "Peperoni, "
        }

        return ingredientes

    }
    var itemMenu: Menu? = null
    //Para mostrar el menu superior
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        //TODO Necesito acceder a la propiedad icon del menu_main para poder cambiarla
        itemMenu=menu
        if (RecyclerAdapterPedido.itemsPedido.isEmpty()) {
            menu.getItem(0).setIcon(R.drawable.carrito)
        }else{
            menu.getItem(0).setIcon(R.drawable.carritolleno2)
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        //Compruebo si el carrito esta lleno y pongo la foto correspondiente
        return when (item.itemId) {
            R.id.action_settings ->  {
                if (RecyclerAdapterPedido.itemsPedido.isEmpty()) {
                    //item.setIcon(R.drawable.carrito)
                    Toast.makeText(this,"El carrito está vacio",Toast.LENGTH_LONG).show()

                }else{
                    //item.setIcon(R.drawable.carritolleno2)
                    var intent = Intent(this, PedidoActivity::class.java)
                    startActivity(intent)
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
