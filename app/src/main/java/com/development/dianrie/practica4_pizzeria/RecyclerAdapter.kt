package com.development.dianrie.practica4_pizzeria

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog
import com.github.javiersantos.materialstyleddialogs.enums.Style


class RecyclerAdapter : RecyclerView.Adapter<RecyclerAdapter.ViewHolder>() {


    //Data Source

    var nombresPizza: Array<String> = arrayOf("Carne",
        "Peperoni", "Piña y Jamón", "Queso", "Salchicha", "Suprema",
        "Vegetariana")
    var descripcionesPizza: Array<String> = arrayOf(
        "Meat Eater\n" +
                "¿Qué estás viendo? Nuestra Meat Eater tiene mucho que ofrecer, queso cien por ciento autentico, pepperoni, jamón, carne, salchicha. Definitivamente es un sueño hecho realidad para los amantes de carne.",
        "Pepperoni\n" +
                "Vamos… ¡te debe encantar la original pepperoni! Nada dice pizza como rebanadas ricas de pepperoni, distribuidas en una salsa de tomate debajo de delicioso queso cien por ciento autentico rodeado por una masa tradicional de mantequilla de ajo. Definitivamente, esto sí que es pizza, ¿acaso no tengo razón?",
        "Piña y Jamón\n" +
                "Tradicional masa de mantequilla de ajo con salsa de tomate, queso 100% autentico, rebanadas de jamón y piña",
        "Queso\n" +
                "Mira, lo sabemos. A veces ‘al natural’ es mejor, y no podemos culparte. Especialmente cuando eso significa el deleitarte con una rebanada de pizza de Queso con salsa de tomate, queso cien por ciento autentico, masa tradicional de mantequilla de ajo, y… pues, eso es todo. Ah, ¡que libertador!",
        "Salchicha\n" +
                "Espera un minuto. ¿Sal…chicha? Hmm, creo que esta es una buena idea. Puedes apostar que nuestra salsa de tomate y salchicha estilo Italiana irían juntos perfectamente. Especialmente cuando se integra nuestro queso cien por ciento autentico y la tradicional masa de ajo. ¿Suena rico? Nosotros también lo creemos.",
        "Suprema\n" +
                "Aquellos que les gusta ponerle un poco más a la vida, nuestra Supreme es para ti. Cantidades ilimitadas de sabroso pepperoni, carne, salchicha y la cantidad justa de cebollas rojas, pimientos verdes, champiñones… ¡uf! Se me fue la respiración…todo esto sobre queso cien por ciento autentico. Creo que ya hablamos demasiado, es hora de comer.",
        "Vegetariana\n" +
                "¿Te gusta vivir al límite? Nuestra recete Veggie es perfecta para aquellos que les encanta todos los colores del espectro de verduras – salsa de tomate, queso cien por ciento autentico, cebollas rojas, pimientos verdes champiñones y aceitunas negras. Creo que aquí la dejamos. Nom-nom-nom."
    )
    var preciosPizza: Array<Double> = arrayOf( 5.0,
        6.0, 8.0, 5.0, 4.0, 7.0,
        8.0, 6.0)
    var nombresImagenes= intArrayOf(
        R.drawable.carne,
        R.drawable.pepperoni,
        R.drawable.pina_jamon,
        R.drawable.queso,
        R.drawable.salchicha,
        R.drawable.suprema,
        R.drawable.vegetariana
        )

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): RecyclerAdapter.ViewHolder {
        val vistaCelda = LayoutInflater.from(p0.context).inflate(R.layout.card_layout,p0,false)
        return ViewHolder(vistaCelda)
    }

    override fun getItemCount(): Int {
        return nombresPizza.size
    }

    override fun onBindViewHolder(p0: RecyclerAdapter.ViewHolder, p1: Int) {
        p0.nombrePizza.text = nombresPizza[p1]
        p0.precioPizza.text = preciosPizza[p1].toString()
        p0.imagenPizza.setImageResource(nombresImagenes[p1])
        p0.textoDescripcion = descripcionesPizza[p1]
        p0.posicion = p1
        p0.imagenFondo = nombresImagenes[p1]

    }

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        var nombrePizza: TextView
        var precioPizza: TextView
        var imagenPizza: ImageView
        var textoDescripcion: String
        var posicion: Int
        var imagenFondo: Int
        var intent = Intent(itemView.context, FormularioActivity::class.java)
        init {
            nombrePizza = itemView.findViewById(R.id.textView)
            precioPizza = itemView.findViewById(R.id.textView3)
            imagenPizza = itemView.findViewById(R.id.imageView)
            textoDescripcion = ""
            posicion = 1
            imagenFondo = R.drawable.carne

            //Listener para la imagen que muestre la descripcion de la pizza
            imagenPizza.setOnClickListener {

                val alerta = MaterialStyledDialog.Builder(itemView.context)
                alerta.setTitle("Ingredientes:")
                alerta.setDescription(textoDescripcion)

                alerta.setHeaderDrawable(imagenFondo)
                alerta.setHeaderColor(R.color.mtrl_btn_bg_color_disabled)

                alerta.setStyle(Style.HEADER_WITH_ICON)

                alerta.setNeutralText("Cancelar")
                alerta.onNeutral { _, _ ->  }

                alerta.setPositiveText("Pedir")
                alerta.onPositive { dialog, which ->
                    Toast.makeText(itemView.context,"Elige un tamaño y los extras que quieras",Toast.LENGTH_LONG).show()
                   // Intent para pasar datos al formulario si se selecciona desde la descripcion
                    //Extras
                    intent.putExtra("Posicion", "${posicion}")
                    itemView.context.startActivity(intent)
                }
                alerta.show()
            }
            //Celda Listener para pasar datos al formulario si se seleciona la celda
            itemView.setOnClickListener {
                //Extras
                //Toast.makeText(itemView.context,"Elige un tamaño y los extras que quieras",Toast.LENGTH_LONG).show()
                intent.putExtra("Posicion", "${posicion}")
                itemView.context.startActivity(intent)

                }

        }
    }
}